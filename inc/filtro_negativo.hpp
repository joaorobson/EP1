#ifndef FILTRO_NEGATIVO_HPP
#define FILTRO_NEGATIVO_HPP
#include "filtro.hpp"
#include "imagem.hpp"

class Filtro_negativo : public Filtro{
public:

	Filtro_negativo();
	void aplicarFiltro_negativo(Imagem* imagem);
};
#endif

