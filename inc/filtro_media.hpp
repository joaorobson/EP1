#ifndef FILTRO_MEDIA_HPP
#define FILTRO_MEDIA_HPP
#include "filtro.hpp"
#include "imagem.hpp"

class Filtro_media : public Filtro{
private:
int limite;
int tamanho;
public:

	Filtro_media();
	void setTamanho(int tamanho);
	void setLimite(int limite);
	void aplicarFiltro_media(Imagem* imagem);
};
#endif

