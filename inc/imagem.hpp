#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>

using namespace std;

class Imagem {
public:
	struct matriz{
		int red;
		int green;
		int blue;
	};
private:
	struct matriz **m;
	char numero_magico[3];
	int altura;
	int largura;
	int valor_max;
	string caminho_abrir;
	string caminho_salvar;
	char comentario[101];
public:
	Imagem();
	//~Imagem();
	void openImagem();
	void createFile();
	void getInfoImagem(string caminho_abrir);
	char getNumero_magico(int i);
	void setNumero_magico(string numero_magico);
	int getMatrizRed(int i,int j);
	int getMatrizGreen(int i,int j);
	int getMatrizBlue(int i,int j);
	void setMatriz(struct matriz** m);
	void setMatrizRed(int i,int j);
	void setCaminho_abrir();
	string getCaminho_abrir();
	void setCaminho_salvar();
	string getCaminho_salvar();
	void imprimirComentario();
	int getAltura();
	void setAltura(int altura);
	int getLargura();
	void setLargura(int largura);
	int getValor_max();
	void setValor_max(int valor_max);
	
	
};
#endif
