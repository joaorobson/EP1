#ifndef FILTRO_POLARIZADO_HPP
#define FILTRO_POLARIZADO_HPP
#include "filtro.hpp"
#include "imagem.hpp"

class Filtro_polarizado : public Filtro{
public:

	Filtro_polarizado();
	void aplicarFiltro_polarizado(Imagem* imagem);
};
#endif

