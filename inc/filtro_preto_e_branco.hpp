#ifndef FILTRO_PRETO_E_BRANCO_HPP
#define FILTRO_PRETO_E_BRANCO_HPP
#include "filtro.hpp"
#include "imagem.hpp"

class Filtro_preto_e_branco : public Filtro{
public:

	Filtro_preto_e_branco();
	void aplicarFiltro_preto_e_branco(Imagem* imagem);
};
#endif

