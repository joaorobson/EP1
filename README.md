

# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

Para aplicar os filtros, o usuário deve possuir no computador ou em um dispositivo de memória móvel alguma imagem ppm. No terminal, deve-se abir a pasta onde encontra-se o projeto por meio do comando "cd", por exemplo, "cd Documentos/EP1". 


Já dentro do diretório, o usuário deve digitar no terminal "make clean","make" e "make run", em sequência. A partir daí, o programa perguntará o local onde se localiza a imagem e o local onde será salva, além de mostrar as opções de filtro existentes a serem aplicadas.


