#include "filtro_polarizado.hpp"
#include "imagem.hpp"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

Filtro_polarizado::Filtro_polarizado(){
}	
	
void Filtro_polarizado::aplicarFiltro_polarizado(Imagem* imagem){
	
	ofstream dados_imagem (imagem->getCaminho_salvar().c_str());
	for(int k=0;k<2;k++){
			dados_imagem << imagem->getNumero_magico(k);
		}
	dados_imagem << endl;
	
	
	dados_imagem << imagem->getLargura()<< endl;
	
	dados_imagem <<  imagem->getAltura() << endl;
	
	
	dados_imagem << imagem->getValor_max() << endl;
				
    
	struct matriz **nm=new struct matriz*[imagem->getAltura()];
	
	for(int i = 0;i < imagem->getAltura(); ++i){
        nm[i] = new matriz[imagem->getLargura()];
    }
	for(int i = 0; i < imagem->getAltura(); i++){
		for(int j = 0; j < imagem->getLargura(); j++){
		       if((unsigned int)imagem->getMatrizRed(i,j) < (unsigned int)(imagem->getValor_max())/2){
		          nm[i][j].red= 0;
		       }
		       else{
		           nm[i][j].red = imagem->getValor_max();
		       }

		       if((unsigned int)imagem->getMatrizGreen(i,j) < (unsigned int)(imagem->getValor_max())/2){
		           nm[i][j].green = 0;
		       }
		       else{
		           nm[i][j].green = imagem->getValor_max();
		       }

		       if((unsigned int)imagem->getMatrizBlue(i,j) < (unsigned int)(imagem->getValor_max())/2){
		       
		           nm[i][j].blue = 0;
		       }
		       else{
		           nm[i][j].blue = imagem->getValor_max();
		       }
    }
}


	for(int i=0;i<imagem->getAltura();i++){	
		for(int j=0;j<imagem->getLargura();j++){
				dados_imagem << (unsigned char)nm[i][j].red;
				dados_imagem << (unsigned char)nm[i][j].green;
				dados_imagem << (unsigned char)nm[i][j].blue;
		}
		
	}
	
	
	dados_imagem.close();
	
		
}

