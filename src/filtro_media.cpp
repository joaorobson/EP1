#include "filtro_media.hpp"
#include "imagem.hpp"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

Filtro_media::Filtro_media(){
limite=0;
tamanho=0;
}	
void Filtro_media::setTamanho(int tamanho){
	this->tamanho=tamanho;
}
void Filtro_media::setLimite(int limite){
	this->limite=limite;
}	
void Filtro_media::aplicarFiltro_media(Imagem* imagem){
	
	
	ofstream dados_imagem (imagem->getCaminho_salvar().c_str());
	for(int k=0;k<2;k++){
			dados_imagem << imagem->getNumero_magico(k);
		}
	dados_imagem << endl;
	
	
	dados_imagem << imagem->getLargura()<< endl;
	
	dados_imagem <<  imagem->getAltura() << endl;
	
	
	dados_imagem << imagem->getValor_max() << endl;
				
    
	struct matriz **nm=new struct matriz*[imagem->getAltura()];
	
	for(int i = 0;i < imagem->getAltura(); ++i){
        nm[i] = new matriz[imagem->getLargura()];
    }
    
   struct matriz **a=new struct matriz*[imagem->getAltura()+limite-1];
	
	for(int i = 0;i < imagem->getAltura()+limite-1; ++i){
        a[i] = new matriz[imagem->getLargura()+limite-1];
    }
   
    int linha=0,coluna=0;
    
    for(int n=((limite-1)/2);n<imagem->getAltura()+1;n++){
   		for(int m=((limite-1)/2);m<imagem->getLargura()+1;m++){
   			a[n][m].red=imagem->getMatrizRed(linha,coluna);
    		a[n][m].green=imagem->getMatrizGreen(linha,coluna);
    		a[n][m].blue=imagem->getMatrizBlue(linha,coluna);
    		coluna++;
    		
    		
    	}
    	  coluna=0;
		  linha++;
    }
    //Coluna da esquerda
    linha=0;
    coluna=0;
    while(linha<imagem->getAltura()+(limite-1)){
    	while(coluna<((limite-1)/2)){
    	a[linha][coluna].red=0;
    	a[linha][coluna].green=0;
    	a[linha][coluna].blue=0;
    	coluna++;
    }
    coluna=0;
    linha++;
    }
    
    //Coluna da direita
    linha=0;
    coluna=imagem->getLargura();
    while(linha<imagem->getAltura()+(limite-1)){
    	while(coluna<imagem->getLargura()+(limite-1)){
    	a[linha][coluna].red=0;
    	a[linha][coluna].green=0;
    	a[linha][coluna].blue=0;
    	coluna++;
    }
    coluna=imagem->getLargura();
    linha++;
    }
    //Linha de cima
    linha=0;
    coluna=0;
    while(linha<((limite-1)/2)){
    	while(coluna<getLargura()+(limite-1)){
    	a[linha][coluna].red=0;
    	a[linha][coluna].green=0;
    	a[linha][coluna].blue=0;
    	coluna++;
    }
    coluna=0;
    linha++;
    }
    
    
    //Linha de baixo
    linha=imagem->getAltura();
    coluna=0;
    while(linha<imagem->getAltura()+(limite-1)){
    	while(coluna<getLargura()+(limite-1)){
    	a[linha][coluna].red=0;
    	a[linha][coluna].green=0;
    	a[linha][coluna].blue=0;
    	coluna++;
    }
    coluna=0;
    linha++;
    }
    
    
    
	struct matriz value;
    int pixel_i=0,pixel_j=0,cont=0;
	
	linha=0;
	coluna=0;
	for(int i = 0; i <imagem->getAltura(); i++){
		for(int j = 0; j <imagem->getLargura(); j++) {
		    value.red =0;
		     value.green=0;
		     value.blue =0;
		   
		        
		    for(int x=0;x<limite;x++){
		                for(int y=0;y<limite;y++){
		                      value.red = (unsigned char)a[linha][coluna].red + value.red;
		                      value.green = (unsigned char)a[linha][coluna].green + value.green;
		                      value.blue =  (unsigned char)a[linha][coluna].blue + value.blue;
		                     coluna++;
		  				 
		                }
		              
		                coluna=coluna-limite;
		       			linha++;
		       }
		       
           
		    
		      
           
		    cont++;
		    coluna=coluna+1;
		     
          
		    value.red   =   (value.red/tamanho);
		 	value.green =(value.green/tamanho);
		  	value.blue  = (value.blue/tamanho);
		  	
		  	//Salvando matriz com filtro
	     nm[pixel_i][pixel_j].red =(unsigned char)value.red;
	      nm[pixel_i][pixel_j].green =  (unsigned char)value.green; 
	       nm[pixel_i][pixel_j].blue = (unsigned char) value.blue; 
	      
         
          linha=linha-limite;
           pixel_j=pixel_j+1;
           
		     cout<<linha;
    }
    cont=0;
    coluna=0;
    linha=linha+1;
    pixel_j=0;
    pixel_i=pixel_i+1;
}

	
	

	for(int i=0;i<imagem->getAltura();i++){	
		for(int j=0;j<imagem->getLargura();j++){
				dados_imagem << (char)nm[i][j].red;
				dados_imagem << (char)nm[i][j].green;
				dados_imagem << (char)nm[i][j].blue;
		}
		
	}
	
	
	dados_imagem.close();
	
		
}

