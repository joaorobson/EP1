#include <iostream>
#include <fstream>
#include "imagem.hpp"
#include "filtro.hpp"
#include "filtro_negativo.hpp"
#include "filtro_polarizado.hpp"
#include "filtro_preto_e_branco.hpp"
#include "filtro_media.hpp"
#include <string>
#include<sstream>
#include<math.h>
#include <cstdio>
using namespace std;

int main(int argc,char ** argv){

	
	int opcao,opcao_masc;
	Imagem *imagem_1 = new Imagem();
	Filtro_preto_e_branco *filtro_1 = new Filtro_preto_e_branco();
	Filtro_negativo *filtro_2 = new Filtro_negativo();
	Filtro_polarizado *filtro_3 = new Filtro_polarizado();
	Filtro_media *filtro_4 = new Filtro_media();
	imagem_1->setCaminho_abrir();
	imagem_1->setCaminho_salvar();
	imagem_1->getInfoImagem(imagem_1->getCaminho_abrir());
	
	
	while(true){
	cout << endl;
	cout << "Escolha uma opção de filtro:" << endl;
		cout << "------------------" << endl;
		cout << "1 - Filtro negativo" << endl;
		cout << "2 - Filtro preto e branco" << endl;
		cout << "3 - Filtro polarizado" << endl;
		cout << "4 - Filtro de média" << endl;
		cout << endl;
		cout << "Opção de filtro desejada: "; 
		cin >> opcao;
		if(opcao<1||opcao>4){
			cout << "Opção inválida.Escolha novamente" << endl;
			
		}
		else{
		
			if(opcao==1){
				filtro_1->aplicarFiltro_preto_e_branco(imagem_1);
				break;
			}
			else if(opcao==2){
				filtro_2->aplicarFiltro_negativo(imagem_1);
				break;
			}
			else if(opcao==3){
				filtro_3->aplicarFiltro_polarizado(imagem_1);
				break;
			}
			else if(opcao==4){
			cout << endl;
				while(true){
				cout << "Escolha uma opção de máscara de filtro para o filtro de média:" << endl;
					cout << "------------------" << endl;
					cout << "1 - Máscara 3x3" << endl;
					cout << "2 - Máscara 5x5" << endl;
					cout << "3 - Máscara 7x7" << endl;
					cout << "Opção de máscara desejada: ";
					cin >> opcao_masc;
					if(opcao_masc<1||opcao_masc>3){
						cout << "Opção inválida.Escolha novamente" << endl;
			
					}
					else{
						if(opcao_masc==1){
							filtro_4->setLimite(3);
							filtro_4->setTamanho(9);
							filtro_4->aplicarFiltro_media(imagem_1);
							break;
						}
						else if(opcao_masc==2){
							filtro_4->setLimite(5);
							filtro_4->setTamanho(25);
							filtro_4->aplicarFiltro_media(imagem_1);
							break;
						}
						else if(opcao_masc==3){
							filtro_4->setLimite(7);
							filtro_4->setTamanho(49);
							filtro_4->aplicarFiltro_media(imagem_1);
							break;
						}
					}
				}	
					
					
		break;
		
		
		}
		
		}
		}
		
		
		
		
		
		
	
	
	
	
	return 0;


}

