#include <iostream>
#include <fstream>
#include "imagem.hpp"
#include <string>
#include<sstream>
#include <cstdlib>

using namespace std;

Imagem::Imagem(){
	
	struct matriz{
		int red;
		int green;
		int blue;
	};
	char numero_magico[3];
	altura =0;
	largura = 0;
	valor_max = 255;
	char comentario[101];
	struct matriz **m;
	
	
}
char Imagem::getNumero_magico(int i){
	return numero_magico[i];
	
}

int Imagem::getAltura(){
	return altura;
}

int Imagem::getLargura(){
	return largura;
}

int Imagem::getValor_max(){
	return valor_max;
}
void Imagem::setCaminho_abrir(){
 	string caminho_abrir;
 	cout << "Digite o caminho do arquivo para abri-lo na forma /doc/arq.ppm: ";
	cin >> caminho_abrir;
	this->caminho_abrir=caminho_abrir;
	
}
string Imagem::getCaminho_abrir(){
	return caminho_abrir;
}	
	

void Imagem::setCaminho_salvar(){
	string caminho_salvar;
	cout << "Digite o caminho do arquivo para salvá-lo no formato ""ppm"": ";
	cin >> caminho_salvar;
	this->caminho_salvar=caminho_salvar;
}

string Imagem::getCaminho_salvar(){
	return caminho_salvar;
}


void Imagem::setNumero_magico(string numero_magico){
	for(int i=0;i<=3;i++){
	this->numero_magico[i]=numero_magico[i];
	}
}

void Imagem::setLargura(int largura){
	this->largura=largura;
}

void Imagem::setAltura(int altura){
	this->altura=altura;
}


void Imagem::setValor_max(int valor_max){
	this->valor_max=valor_max;
}

void Imagem::setMatriz(struct matriz **a){
	m=new struct matriz*[getAltura()];
		for(int i = 0;i < getAltura(); ++i){
        m[i] = new matriz[getLargura()];
    }
    
	for(int i=0;i<getAltura();i++){
		for(int j=0;j<getLargura();j++){
				m[i][j].red  = a[i][j].red;
				m[i][j].green= a[i][j].green;
				m[i][j].blue = a[i][j].blue;		
		}
	}
	
}	
int Imagem::getMatrizRed(int i,int j){
	return m[i][j].red;
}

int Imagem::getMatrizGreen(int i,int j){
	return m[i][j].green;
}

int Imagem::getMatrizBlue(int i,int j){
	return m[i][j].blue;
}

void Imagem::getInfoImagem(string caminho_abrir){
	ifstream imagem (caminho_abrir.c_str());
	
	if(!imagem.is_open()){
		cout << "Não foi possível abrir arquivo! Programa será fechado!\n";
		imagem.clear();
	}
	
	
	
	string linhas,colunas,max_tonalidade,linha,f;
	int n=0,i=0,j=0,p=0,repetir;
	char temp[2];
	
	

	
	while(true){
		temp[0]=imagem.get();
		f[i]=temp[0];
		
		if(f[i]=='\n'){
			numero_magico[i]=f[i];
			break;
		}
		numero_magico[i]=f[i];
		i++;
	}
	cout << "Número mágico: ";
		for(int k=0;k<i;k++){
			cout << numero_magico[k];
			
		}
		cout << endl;
		setNumero_magico(numero_magico);
	
	

	i=0;
		
		//Ignorar os comentarios da imagem
		
		temp[0]=imagem.get();
		
		
		if(temp[0]=='#'){
			repetir =1;
		while(repetir == 1){
			comentario[i]=temp[0];
			cout << comentario[i];
			i++;
			
			while(temp[0]!='\n'){
				temp[0]=imagem.get();
				comentario[i]=temp[0];
				cout << comentario[i];
				i++;
				if(i==100){
				
					i=0;
				}
				
				}
				
					temp[0]=imagem.get();
					if(temp[0]=='#'){
							repetir=1;
					}
					else{
						repetir = 0;
					}
				
					
				}
				
					colunas[j]=temp[0];
					j++;
					while(true){
						temp[0]=imagem.get();
						colunas[j]=temp[0];
						j++;
						if(temp[0]==' '||temp[0]=='\n'){
							temp[0]=imagem.get();
							linhas[n]=temp[0];
							n++;
							while(true){
								temp[0]=imagem.get();
								linhas[n]=temp[0];
								n++;
								if(temp[0]=='\n'){
									temp[0]=imagem.get();
									max_tonalidade[p]=temp[0];
									p++;
									while(true){
										temp[0]=imagem.get();
										max_tonalidade[p]=temp[0];
										p++;
										if(temp[0]=='\n'){
											goto imprimir;
										}
									}
								}
							}
						}
						
					
					}	
				
			
				
		}
		
		else{
						
			colunas[j]=temp[0];
			j++;
					
					while(true){
						temp[0]=imagem.get();
						colunas[j]=temp[0];
						j++;
						if(temp[0]==' '||temp[0]=='\n'){
							temp[0]=imagem.get();
							linhas[n]=temp[0];
							n++;
							while(true){
								temp[0]=imagem.get();
								linhas[n]=temp[0];
								n++;
								if(temp[0]=='\n'){
									temp[0]=imagem.get();
									max_tonalidade[p]=temp[0];
									p++;
									while(true){
										temp[0]=imagem.get();
										max_tonalidade[p]=temp[0];
										p++;
										if(temp[0]=='\n'){
											goto imprimir;
										}
									}
								}
							}
						}
						
					
					}	
		}
					imprimir:
					
					cout << "Largura da imagem: ";
					for(int k=0;k<j;k++){
						cout << colunas[k];
						
					}
					cout << "Altura da imagem: ";
					for(int k=0;k<n;k++){
						cout << linhas[k];
						
					}
					cout << "Tonalidade máxima da imagem: ";
					for(int k=0;k<p;k++){
						cout << max_tonalidade[k];
						
					}
					
					cout << endl;
					largura=atoi(colunas.c_str());
					setLargura(largura);
	
		
					altura=atoi(linhas.c_str());
					setAltura(altura);
	
	
					valor_max=atoi(max_tonalidade.c_str());
					setValor_max(valor_max);
	
	
	
	
	
	
	
	
	
	
	//Alocando matriz dinâmica
	struct matriz **a=new struct matriz*[altura];
	
	for(int i = 0;i < altura; ++i){
        a[i] = new matriz[largura];
       
    }
	struct matriz **nm=new struct matriz*[altura];
	
	for(int i = 0;i < altura; ++i){
        nm[i] = new matriz[largura];
       
    }
	char c[3];
	for(int i=0;i<altura;i++){
		for(int j=0;j<largura;j++){
				c[0] = imagem.get();
				c[1] = imagem.get();
				c[2] = imagem.get();
				a[i][j].red  = c[0];
				a[i][j].green= c[1];
				a[i][j].blue = c[2];		
		}
	}
	setMatriz(a);
	
	
	

	
		
}



